package com.galileev.javassist;

import javassist.CtClass;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DefineNewClassTest extends JavassistBaseTest {
  @Test
  public void test() throws Exception {
    CtClass cc = classPool.makeClass(className());
    cc.toClass().newInstance();
  }

  @Test
  public void makesClassNotInterface() throws Exception {
    CtClass cc = classPool.makeClass(className());
    assertFalse(cc.toClass().isInterface());
  }

  @Test
  public void makeInterface() throws Exception {
    CtClass cc = classPool.makeInterface(className());
    assertTrue(cc.toClass().isInterface());
  }
}
