package com.galileev.javassist;

import com.galileev.cglib.Bean;
import javassist.*;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

public class TranslatorTest extends JavassistBaseTest {
  @Before
  public void ensureClassNotPooled() throws Exception {
    classPool.get(Bean.class.getName()).detach();
  }

  @Test
  public void onLoadIsInvokedOncePerClassName() throws Exception {
    Loader loader = new Loader(classPool);
    final int[] onLoadInvocationCount = {0};
    loader.addTranslator(classPool, new Translator() {
      @Override
      public void start(ClassPool pool) throws NotFoundException, CannotCompileException {
        onLoadInvocationCount[0] = 0;
      }

      @Override
      public void onLoad(ClassPool pool, String classname) throws NotFoundException, CannotCompileException {
        onLoadInvocationCount[0]++;
      }
    });
    loader.loadClass(Bean.class.getName());
    loader.loadClass(Bean.class.getName());
    loader.loadClass(Bean.class.getName());
    assertEquals(1, onLoadInvocationCount[0]);
  }

  @Test
  public void modifyClass() throws Exception {
    Loader loader = new Loader(classPool);
    loader.addTranslator(classPool, new Translator() {
      @Override
      public void start(ClassPool pool) throws NotFoundException, CannotCompileException {
      }

      @Override
      public void onLoad(ClassPool pool, String className) throws NotFoundException, CannotCompileException {
        if (className.equals("com.galileev.cglib.Bean")) {
          CtClass cc = pool.get(className);
          CtMethod getValue = cc.getDeclaredMethod("getValue");
          getValue.insertBefore("this.value = this.value + this.value;");
          cc.freeze();
        }
      }
    });
    Class<?> modifiedClass = loader.loadClass(Bean.class.getName());
    Object o = modifiedClass.newInstance();
    Method m = modifiedClass.getDeclaredMethod("setValue", String.class);
    m.invoke(o, "x");
    m = modifiedClass.getDeclaredMethod("getValue");
    assertEquals("xx", m.invoke(o));
  }
}