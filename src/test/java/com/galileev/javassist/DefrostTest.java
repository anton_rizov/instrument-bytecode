package com.galileev.javassist;

import javassist.CtClass;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DefrostTest extends JavassistBaseTest {
  @Test
  public void main() throws Exception {
    CtClass cc = classPool.makeClass(className());
    cc.freeze();
    assertTrue(cc.isFrozen());
    cc.defrost();
    assertFalse(cc.isFrozen());
  }
}
