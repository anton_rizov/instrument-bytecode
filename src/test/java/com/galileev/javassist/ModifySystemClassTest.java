package com.galileev.javassist;

import javassist.*;
import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.assertThat;

public class ModifySystemClassTest {
  private static final String FIELD_NAME = "hiddenValue";

  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();

  @Test
  public void modifyString() throws Exception {
    CtClass cc = addNewFieldToStringClass();
    String directory = writeToTemporaryFolder(cc);
    String out = runMainInNewJVMUsingModifiedString(directory);
    assertThat(out, CoreMatchers.containsString(FIELD_NAME));
  }

  private String runMainInNewJVMUsingModifiedString(String directory) throws IOException {
    ProcessBuilder processBuilder = new ProcessBuilder();
    processBuilder.command("java", "-Xbootclasspath/p:" + directory,
                           "-cp", System.getProperty("java.class.path"),
                           getClass().getName());
    Process process = processBuilder.start();
    return slurp(process.getInputStream());
  }

  private CtClass addNewFieldToStringClass() throws NotFoundException, CannotCompileException, IOException {
    ClassPool pool = ClassPool.getDefault();
    CtClass cc = pool.get("java.lang.String");
    CtField f = new CtField(CtClass.intType, FIELD_NAME, cc);
    f.setModifiers(Modifier.PUBLIC);
    cc.addField(f);
    return cc;
  }

  private String writeToTemporaryFolder(CtClass cc) throws IOException, CannotCompileException {
    String directory = temporaryFolder.newFolder().getAbsolutePath();
    cc.writeFile(directory);
    return directory;
  }

  private static String slurp(InputStream in) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(in));
    StringBuilder builder = new StringBuilder();
    String line;
    while ((line = br.readLine()) != null) {
      builder.append(line);
      builder.append(System.getProperty("line.separator"));
    }
    return builder.toString();
  }

  public static void main(String[] args) throws Exception {
    System.out.println(getNewField());
  }

  private static String getNewField() throws NoSuchFieldException {
    return String.class.getField(FIELD_NAME).getName();
  }
}
