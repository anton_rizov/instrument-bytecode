package com.galileev.javassist;

import javassist.CtClass;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

public class ChangeClassNameTest extends JavassistBaseTest {
  private String name = "com.galileev.cglib.Bean";
  private CtClass cc;

  @Before
  public void setUp() throws Exception {
    cc = classPool.get(name);
  }

  @Test
  public void main() throws Exception {
    assertEquals(name, cc.getName());
    String newName = className();
    cc.setName(newName);
    assertEquals(newName, cc.toClass().getName());
  }

  @Test
  public void canAccessOriginalClass() throws Exception {
    assertEquals(name, cc.getName());
    String newName = className();
    cc.setName(newName);
    assertSame(cc, classPool.get(newName));
    assertNotSame(cc, classPool.get(name));
  }
}
