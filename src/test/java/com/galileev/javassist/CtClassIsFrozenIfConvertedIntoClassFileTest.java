package com.galileev.javassist;

import javassist.ClassPool;
import javassist.CtClass;
import org.junit.Rule;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;

import java.lang.reflect.Method;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(Theories.class)
public class CtClassIsFrozenIfConvertedIntoClassFileTest {
  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  @DataPoints
  public static final String[] METHOD_NAMES = {"toClass", "writeFile", "toBytecode"};

  private ClassPool classPool = ClassPool.getDefault();

  @Theory
  public void ctClassIsFrozenAfterMethod(String methodName) throws Exception {
    CtClass cc = classPool.makeClass(this.getClass().getName() + "_" + methodName);
    assertFalse(cc.isFrozen());
    if (methodName.equals("writeFile")) {
      Method m = cc.getClass().getMethod(methodName, String.class);
      m.invoke(cc, folder.newFolder().getAbsolutePath());
    } else {
      Method m = cc.getClass().getMethod(methodName);
      m.invoke(cc);
    }
    assertTrue(cc.isFrozen());
  }
}
