package com.galileev.javassist;

import javassist.CtClass;
import org.junit.Test;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

public class RemoveClassFromClassPoolTest extends JavassistBaseTest {
  @Test
  public void main() throws Exception {
    String name = "com.galileev.cglib.Bean";
    CtClass cc = classPool.get(name);
    assertSame(cc, classPool.get(name));

    cc.detach();

    assertNotSame(cc, classPool.get(name));
  }
}
