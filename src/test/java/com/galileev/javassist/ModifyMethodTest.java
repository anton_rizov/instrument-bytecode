package com.galileev.javassist;

import javassist.CannotCompileException;
import javassist.CtClass;
import javassist.CtMethod;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;


public class ModifyMethodTest extends JavassistBaseTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  private CtClass cc;
  private CtMethod m;

  @Before
  public void setUp() throws CannotCompileException {
    cc = classPool.makeClass(className());
    m = new CtMethod(CtClass.intType, "m", new CtClass[]{CtClass.intType, CtClass.intType}, cc);
    m.setBody("return $2;");
    cc.addMethod(m);
  }

  @Test
  public void insertBefore() throws Exception {
    m.insertBefore("return $1;");
    Object value = callM(1, 2);
    assertEquals(1, value);
  }

  @Test
  public void insertAfter() throws Exception {
    m.insertAfter("return $1;");
    Object value = callM(1, 2);
    assertEquals(1, value);
  }

  @Test
  public void accessReturnValueInInsertAfter() throws Exception {
    m.insertAfter("return $_ + $_;");
    assertEquals(4, callM(1, 2));
  }

  @Test
  public void allActualParameters() throws Exception {
    CtMethod x = new CtMethod(CtClass.intType, "x", new CtClass[]{CtClass.intType, CtClass.intType}, cc);
    x.setBody("return $1;");
    cc.addMethod(x);

    m.setBody("return x($$);");
    assertEquals(1, callM(1, 2));
  }

  @Test
  public void invalidCode() throws Exception {
    expectedException.expect(CannotCompileException.class);
    expectedException.expectMessage(";");
    expectedException.expectMessage("missing");
    m.setBody("return 2");
  }

  @Test
  public void wrapperType() throws Exception {
    m.setBody("{Integer x = ($w)10; return x.intValue();}");
    assertEquals(10, callM(1, 2));
  }

  @Test
  public void autoboxingWorks() throws Exception {
    expectedException.expect(VerifyError.class);
    m.setBody("{Integer x = 10; return x.intValue();}");
    callM(0, 2);
  }

  @Test
  public void resultType() throws Exception {
    CtMethod x = new CtMethod(CtClass.doubleType, "x", new CtClass[0], cc);
    x.setBody("return 7.3;");
    cc.addMethod(x);

    m.setBody("return ($r)x();");
    assertEquals(7, callM());
  }

  @Test
  public void signature() throws Exception {
    m.insertBefore("for (int i = 0; i < $sig.length; i++) System.out.println($sig[i].getName());");

    String out = callMAndCaptureOutput();

    assertEquals(String.format("int%nint%n"), out);
  }

  @Test
  public void foreachLoop() throws Exception {
    expectedException.expect(CannotCompileException.class);
    expectedException.expectMessage(";");
    expectedException.expectMessage("missing");
    m.insertBefore("for (int e : new int[]{1, 2, 3, 4}) ;");
    assertEquals(10, callM());
  }

  private Object callM() throws Exception {
    return callM(0, 1);
  }

  private String callMAndCaptureOutput() throws Exception {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    System.setOut(new PrintStream(out));

    callM();

    System.setOut(null);

    return out.toString();
  }

  @Test
  public void throwException() throws Exception {
    expectedException.expect(InvocationTargetException.class);
    m.insertBefore("throw new java.io.IOException();");
    callM();
  }

  @Test
  public void addCatch() throws Exception {
    m.insertBefore("throw new java.io.IOException();");
    String message = "Caught IOException";
    m.addCatch("{System.out.println(\"" + message + "\"); return 1;}", classPool.get("java.io.IOException"));

    String output = callMAndCaptureOutput();
    assertEquals(String.format("%s%n", message), output);
  }

  private Object callM(int arg1, int arg2) throws Exception {
    Class<?> clazz = cc.toClass();
    Object instance = clazz.newInstance();
    Method m = clazz.getDeclaredMethod("m", int.class, int.class);
    return m.invoke(instance, arg1, arg2);
  }
}
