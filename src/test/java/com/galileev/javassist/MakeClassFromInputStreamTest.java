package com.galileev.javassist;

import javassist.CtClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class MakeClassFromInputStreamTest extends JavassistBaseTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();
  
/*
public class hello{
  public String say(){
    return "hello";
  }
}

javac hello.java

od -An -t d1 hello.class|sed -e 's/\(\([0-9]\)[^0-9]\)/\2, /g'|sed -e 's/$/,/'
*/
  private static final byte[] HELLO_CLASS_FILE = {
    -54,   -2,  -70,  -66,    0,    0,    0,   51,    0,   16,   10,    0,    4,    0,   13,    8,
      0,   14,    7,    0,   14,    7,    0,   15,    1,    0,    6,   60,  105,  110,  105,  116,
     62,    1,    0,    3,   40,   41,   86,    1,    0,    4,   67,  111,  100,  101,    1,    0,
     15,   76,  105,  110,  101,   78,  117,  109,   98,  101,  114,   84,   97,   98,  108,  101,
      1,    0,    3,  115,   97,  121,    1,    0,   20,   40,   41,   76,  106,   97,  118,   97,
     47,  108,   97,  110,  103,   47,   83,  116,  114,  105,  110,  103,   59,    1,    0,   10,
     83,  111,  117,  114,   99,  101,   70,  105,  108,  101,    1,    0,   10,  104,  101,  108,
    108,  111,   46,  106,   97,  118,   97,   12,    0,    5,    0,    6,    1,    0,    5,  104,
    101,  108,  108,  111,    1,    0,   16,  106,   97,  118,   97,   47,  108,   97,  110,  103,
     47,   79,   98,  106,  101,   99,  116,    0,   33,    0,    3,    0,    4,    0,    0,    0,
      0,    0,    2,    0,    1,    0,    5,    0,    6,    0,    1,    0,    7,    0,    0,    0,
     29,    0,    1,    0,    1,    0,    0,    0,    5,   42,  -73,    0,    1,  -79,    0,    0,
      0,    1,    0,    8,    0,    0,    0,    6,    0,    1,    0,    0,    0,    1,    0,    1,
      0,    9,    0,   10,    0,    1,    0,    7,    0,    0,    0,   27,    0,    1,    0,    1,
      0,    0,    0,    3,   18,    2,  -80,    0,    0,    0,    1,    0,    8,    0,    0,    0,
      6,    0,    1,    0,    0,    0,    3,    0,    1,    0,   11,    0,    0,    0,    2,    0,
     12,
  };

  @Test
  public void main() throws Exception {
    CtClass ctClass = classPool.makeClass(new ByteArrayInputStream(HELLO_CLASS_FILE));
    assertEquals("hello", ctClass.toClass().getName());
  }

  @Test
  public void brokenClassFile() throws Exception {
    byte[] bytes = Arrays.copyOf(HELLO_CLASS_FILE, HELLO_CLASS_FILE.length);
    bytes[0] ^= bytes[0];

    expectedException.expect(IOException.class);
    expectedException.expectMessage("bad magic number");
    classPool.makeClass(new ByteArrayInputStream(bytes));
  }

  @Test
  public void twice() throws Exception {
    expectedException.expect(RuntimeException.class);
    expectedException.expectMessage("frozen");

    classPool.makeClass(new ByteArrayInputStream(HELLO_CLASS_FILE));
    classPool.makeClass(new ByteArrayInputStream(HELLO_CLASS_FILE));
  }
}
