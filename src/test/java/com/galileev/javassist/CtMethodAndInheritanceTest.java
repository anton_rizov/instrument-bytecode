package com.galileev.javassist;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class CtMethodAndInheritanceTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  private ClassPool pool = ClassPool.getDefault();

  @Test
  public void inheritedMethodsAreNotDeclared() throws Exception {
    expectedException.expect(javassist.NotFoundException.class);
    expectedException.expectMessage("is not found");
    getMoveMethod("com.galileev.javassist.ColorPoint");
  }

  @Test
  public void inherit() throws Exception {
    CtMethod moveInPoint = getMoveMethod("com.galileev.javassist.Point");
    CtMethod moveInColorPoint = getMoveMethodUsingSearch("com.galileev.javassist.ColorPoint");
    assertEquals(moveInPoint, moveInColorPoint);
    assertSame(moveInPoint, moveInColorPoint);
  }

  private javassist.CtMethod getMoveMethod(String className) throws NotFoundException {
    CtClass cc = pool.getCtClass(className);
    return cc.getDeclaredMethod("move", new CtClass[]{});
  }

  private javassist.CtMethod getMoveMethodUsingSearch(String className) throws NotFoundException {
    CtClass cc = pool.getCtClass(className);
    CtMethod[] methods = cc.getMethods();
    for (CtMethod method : methods) {
      if (method.getName().equals("move") && method.getParameterTypes().length == 0)
        return method;
    }
    throw new IllegalArgumentException("Class " + className + " has no move method");
  }

  @Test
  public void override() throws Exception {
    CtMethod moveInPoint = getMoveMethod("com.galileev.javassist.Point");
    CtMethod moveInMegaPoint = getMoveMethod("com.galileev.javassist.MegaPoint");
    assertEquals(moveInPoint, moveInMegaPoint);
    assertNotSame(moveInPoint, moveInMegaPoint);
  }
}

@SuppressWarnings("UnusedDeclaration")
class Point {
  public void move() {
  }
}

@SuppressWarnings("UnusedDeclaration")
class ColorPoint extends Point {
}

@SuppressWarnings("UnusedDeclaration")
class MegaPoint extends Point {
  @Override
  public void move() {
  }
}