package com.galileev.javassist;

import javassist.CtClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class RenameFrozenClassForDefiningNewClassTest extends JavassistBaseTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  private String name = "com.galileev.cglib.Bean";

  @Test
  public void cannotRenameFrozenClass() throws Exception {
    expectedException.expect(RuntimeException.class);
    expectedException.expectMessage("is frozen");

    CtClass cc = classPool.get(name);
    cc.freeze();
    cc.setName(className());
  }

  @Test
  public void getAndRename() throws Exception {
    CtClass cc = classPool.get(name);
    cc.freeze();

    String newName = className();
    CtClass cc1 = classPool.getAndRename(name, newName);
    assertEquals(newName, cc1.getName());

    assertNotSame(cc, cc1);
  }
}
