package com.galileev.javassist;

import com.galileev.cglib.Bean;
import javassist.CannotCompileException;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.Loader;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

public class ModifyLoadedClassTest extends JavassistBaseTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();
  private CtClass cc;

  @Before
  public void setUp() throws Exception {
    Bean loaded = new Bean();
    cc = classPool.get(loaded.getClass().getName());
    cc.defrost();

    CtMethod getValue = cc.getDeclaredMethod("getValue");
    getValue.insertBefore("this.value = this.value + this.value;");
  }

  @After
  public void tearDown() throws Exception {
    cc.detach();
  }

  @Test
  public void cannotModifyLoadedClass() throws Exception {
    expectedException.expect(CannotCompileException.class);
    expectedException.expectMessage("java.lang.LinkageError");

    cc.toClass().newInstance();
  }

  @Test
  public void useLoader() throws Exception {
    expectedException.expect(ClassCastException.class);
    expectedException.expectMessage("Bean cannot be cast to ");

    @SuppressWarnings("UnusedDeclaration") Bean ignore = (Bean) classPool.toClass(cc, new Loader(classPool), null).newInstance();
  }

  @Test
  public void useModifiedVersion() throws Exception {
    Class<?> clazz = classPool.toClass(cc, new Loader(classPool), null);

    Object o = clazz.newInstance();

    Method m = clazz.getDeclaredMethod("setValue", String.class);
    m.invoke(o, "x");

    m = clazz.getDeclaredMethod("getValue");
    assertEquals("xx", m.invoke(o));
  }
}