package com.galileev.javassist;

import javassist.CtClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import static org.junit.Assert.assertFalse;

public class DebugWriteFileDoesNotFreezeTest extends JavassistBaseTest {
  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  @Test
  public void main() throws Exception {
    CtClass cc = classPool.makeClass(className());
    assertFalse(cc.isFrozen());
    cc.debugWriteFile(folder.newFolder().getAbsolutePath());
    assertFalse(cc.isFrozen());
  }
}
