package com.galileev.javassist;

import com.galileev.cglib.Greeter;
import javassist.CtClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ChangeSuperclassTest extends JavassistBaseTest {
  @Test
  public void main() throws Exception {
    CtClass cc = classPool.get("com.galileev.cglib.Bean");
    cc.setName(className());
    assertEquals(classPool.get("java.lang.Object"), cc.getSuperclass());
    cc.setSuperclass(classPool.get("com.galileev.cglib.Greeter"));
    Class beanClass = cc.toClass();
    assertTrue(Greeter.class.isAssignableFrom(beanClass));
  }

  @Test(expected = javassist.CannotCompileException.class)
  public void cannotCreateInstanceOfModifiedClassIfLoaded() throws Exception {
    Object anonymous = new Object() {
    };
    CtClass cc = classPool.get(anonymous.getClass().getName());
    cc.setSuperclass(classPool.get("com.galileev.cglib.Greeter"));
    cc.toClass().newInstance();
  }
}
