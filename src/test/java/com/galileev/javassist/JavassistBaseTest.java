package com.galileev.javassist;

import javassist.ClassPool;
import org.junit.Rule;
import org.junit.rules.TestName;

public abstract class JavassistBaseTest {
  @Rule
  public TestName testName = new TestName();

  protected final ClassPool classPool = ClassPool.getDefault();

  protected String className() {
    return this.getClass().getName() + "_" + testName.getMethodName();
  }
}
