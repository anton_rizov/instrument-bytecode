package com.galileev.javassist;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

public class EditMethodSourceTest extends JavassistBaseTest {
  private ClassPool classPool = ClassPool.getDefault();
  private CtClass cc;
  private CtMethod x;

  @Before
  public void setUp() throws Exception {
    cc = classPool.makeClass(className());
    CtMethod m = new CtMethod(CtClass.intType, "m", new CtClass[0], cc);
    m.setBody("return -1;");
    cc.addMethod(m);
    x = new CtMethod(CtClass.intType, "x", new CtClass[0], cc);
    x.setBody("return m() + 1;");
    cc.addMethod(x);
  }

  @Test
  public void replaceMethodCallWithConstant() throws Exception {
    x.instrument(new ExprEditor() {
      @Override
      public void edit(MethodCall m) throws CannotCompileException {
        if (m.getMethodName().equals("m") && m.getClassName().equals(className()))
          m.replace("{$_ = 10;}");
      }
    });
    assertEquals(11, callX());
  }

  @Test
  public void proceed() throws Exception {
    x.instrument(new ExprEditor() {
      @Override
      public void edit(MethodCall m) throws CannotCompileException {
        if (m.getMethodName().equals("m") && m.getClassName().equals(className()))
          m.replace("{$_ = 10 + $proceed($$);}");
      }
    });
    assertEquals(10, callX());
  }

  private Object callX() throws Exception {
    Class<?> clazz = cc.toClass();
    Object o = clazz.newInstance();
    Method x = clazz.getDeclaredMethod("x");
    return x.invoke(o);
  }
}

