package com.galileev.cglib;

import net.sf.cglib.util.StringSwitcher;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StringSwitcherTest {
  private String[] strings = {"one", "two"};
  private int[] ints = {10, 20};
  private StringSwitcher stringSwitcher = StringSwitcher.create(strings, ints, true);

  @Test
  public void existingMapping() {
    for (int i = 0; i < strings.length; i++)
      assertEquals(ints[i], stringSwitcher.intValue(strings[i]));
  }

  @Test
  public void minusOneForNonExistingStrings() {
    String s = makeNonExistingString();
    assertEquals(-1, stringSwitcher.intValue(s));
  }

  private String makeNonExistingString() {
    StringBuilder builder = new StringBuilder();
    for (String s : strings)
      builder.append(s);
    return builder.toString();
  }

  @Test
  public void fasterButUndefined() {
    stringSwitcher = StringSwitcher.create(strings, ints, false);
    int result = stringSwitcher.intValue(makeNonExistingString());
    //noinspection ConstantConditions
    assertTrue(result == -1 || result != -1);
  }
}
