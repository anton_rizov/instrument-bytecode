package com.galileev.cglib;

import net.sf.cglib.core.KeyFactory;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class KeyFactoryTest {
  private MyKeyFactory keyFactory = (MyKeyFactory) KeyFactory.create(MyKeyFactory.class);

  private interface MyKeyFactory {
    Object newInstance(String x0, int x1);
  }

  @Test
  public void usableAsMapKey() throws Exception {
    String value = "Hello cglib!";

    Map<Object, String> map = new HashMap<Object, String>();
    map.put(makeKey(), value);

    assertEquals(value, map.get(makeKey()));
  }

  private Object makeKey() {
    return keyFactory.newInstance("foo", 42);
  }

  @Test
  public void differentInstances() {
    assertNotSame(makeKey(), makeKey());
  }

  interface BrokenKeyFactory {
    Object wrongName(String x0, String x1);
  }

  @Test(expected = java.lang.IllegalArgumentException.class)
  public void methodMustBeNamedNewInstance() {
    KeyFactory.create(BrokenKeyFactory.class);
  }

  interface BrokenKeyFactory2 {
    Integer newInstance(String x0, String x1);
  }

  @Test(expected = java.lang.IllegalArgumentException.class)
  public void methodMustReturnObject() {
    KeyFactory.create(BrokenKeyFactory.class);
  }
}
