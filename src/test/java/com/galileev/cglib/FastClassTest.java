package com.galileev.cglib;

import net.sf.cglib.reflect.FastClass;
import net.sf.cglib.reflect.FastMethod;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class FastClassTest {
  @Test
  public void test() throws Exception {
    FastClass fastClass = FastClass.create(Bean.class);
    FastMethod fastMethod = fastClass.getMethod(Bean.class.getMethod("getValue"));
    Bean bean = new Bean();
    bean.setValue("Hello cglib!");
    assertEquals(bean.getValue(), fastMethod.invoke(bean, new Object[0]));
  }
}
