package com.galileev.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.LazyLoader;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class LazyLoaderEnhancerTest {
  private Greeter greeter;
  private int loadObjectInvocationCount=0;

  @Before
  public void initGreeter() {
    greeter = createEnhancedGreeter();
  }

  private Greeter createEnhancedGreeter() {
    Enhancer enhancer = new Enhancer();
    enhancer.setSuperclass(Greeter.class);
    enhancer.setCallback(new LazyLoader() {
      @Override
      public Object loadObject() throws Exception {
        ++loadObjectInvocationCount;
        return new Greeter();
      }
    });
    return (Greeter) enhancer.create();
  }
  @Test
  public void loadObjectIsNotInvokedBeforeFirstMethod() {
    assertEquals(0, loadObjectInvocationCount);
    greeter.sayHello();
    assertEquals(1, loadObjectInvocationCount);
  }

  @Test
  public void loadObjectIsInvokedOnlyOnce() {
    greeter.sayHello();
    //noinspection ResultOfMethodCallIgnored
    greeter.hashCode();
    assertEquals(1, loadObjectInvocationCount);
  }

  @Test
  public void sayHelloIsNotEnhanced() {
    assertEquals(new Greeter().sayHello(), greeter.sayHello());
  }

  @Test
  public void toStringIsNotEnhanced() {
    assertNotEquals(new Greeter().toString(), greeter.toString());
  }

  @Test
  public void notSameClass() {
    assertNotEquals(Greeter.class, greeter.getClass());
  }
}
