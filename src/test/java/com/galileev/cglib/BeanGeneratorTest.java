package com.galileev.cglib;

import net.sf.cglib.beans.BeanGenerator;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

public class BeanGeneratorTest {
  @Test
  public void testBeanGenerator() throws Exception {
    BeanGenerator beanGenerator = new BeanGenerator();
    beanGenerator.addProperty("value", String.class);
    Object myBean = beanGenerator.create();

    String value = "Hello cglib!";

    Method setter = myBean.getClass().getMethod("setValue", String.class);
    setter.invoke(myBean, value);

    Method getter = myBean.getClass().getMethod("getValue");
    assertEquals(value, getter.invoke(myBean));
  }
}
