package com.galileev.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MethodInterceptorEnhancerTest {
  private static final String REPLACEMENT = "Hello cglib!";

  private Greeter greeter;

  @Before
  public void initGreeter() {
    greeter = createEnhancedGreeter();
  }

  private Greeter createEnhancedGreeter() {
    Enhancer enhancer = new Enhancer();
    enhancer.setSuperclass(Greeter.class);
    enhancer.setCallback(new MethodInterceptor() {
      @Override
      public Object intercept(Object object, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        if (method.getDeclaringClass() != Object.class && method.getReturnType() == String.class) {
          return REPLACEMENT;
        } else {
          return proxy.invokeSuper(object, args);
        }
      }
    });
    return (Greeter) enhancer.create();
  }

  @Test
  public void sayHelloIsEnhanced() {
    assertEquals(REPLACEMENT, greeter.sayHello());
  }

  @Test
  public void toStringIsNotEnhanced() {
    assertNotEquals(REPLACEMENT, greeter.toString());
  }

  @Test
  public void hashCodeIsNotEnhanced() {
    assertNotEquals(REPLACEMENT, greeter.hashCode());
  }

  @Test
  public void notSameClass() {
    assertNotEquals(Greeter.class, greeter.getClass());
  }
}
