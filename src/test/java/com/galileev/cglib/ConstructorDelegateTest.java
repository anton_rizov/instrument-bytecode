package com.galileev.cglib;

import net.sf.cglib.reflect.ConstructorDelegate;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class ConstructorDelegateTest {
  @Rule
  public ExpectedException exception = ExpectedException.none();

  public interface Factory {
    Object newInstance();
  }

  @Test
  public void simpleCase() throws Exception {
    test(Factory.class);
  }

  private void test(Class<? extends Factory> factoryInterface) {
    Factory constructorDelegate = (Factory) ConstructorDelegate.create(Bean.class, factoryInterface);
    Bean bean = (Bean) constructorDelegate.newInstance();
    assertTrue(Bean.class.isAssignableFrom(bean.getClass()));
  }

  private interface NonPublic extends Factory{
    Object newInstance();
  }

  @Test
  public void interfaceDoesNotHaveToBePublic() {
    test(NonPublic.class);
  }

  @SuppressWarnings("UnusedDeclaration")
  interface TwoMethods extends Factory {
    Object newInstance();
    Object make();
  }

  @Test
  public void moreThanOneMethod() {
    exception.expect(IllegalArgumentException.class);
    exception.expectMessage(startsWith("expecting exactly 1 method in interface"));
    test(TwoMethods.class);
  }


  interface XFactory {
    X newInstance(Object arg);
  }

  static class X {
    public final Object arg;

    X(Object arg) {
      this.arg = arg;
    }
  }

  @Test
  public void arguments() {
    XFactory factory = (XFactory) ConstructorDelegate.create(X.class, XFactory.class);
    String arg = "Hello world!";
    X instance = factory.newInstance(arg);
    assertSame(arg, instance.arg);
  }
}
