package com.galileev.cglib;

import net.sf.cglib.beans.BeanMap;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BeanMapTest {
  private Bean bean = new Bean();
  private BeanMap map = BeanMap.create(bean);

  @Test
  public void get() throws Exception {
    bean.setValue("Hello cglib!");
    assertEquals(bean.getValue(), map.get("value"));
  }

  @Test
  public void put() throws Exception {
    map.put("value", bean.getValue() + bean.getValue());
    assertEquals(map.get("value"), bean.getValue());
  }
}
