package com.galileev.cglib;

import net.sf.cglib.beans.BulkBean;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BulkBeanTest {
  @Test
  public void test() throws Exception {
    BulkBean bulkBean = BulkBean.create(Bean.class,
                                        new String[]{"getValue"},
                                        new String[]{"setValue"},
                                        new Class[]{String.class});
    Bean bean = new Bean();
    bean.setValue("Hello world!");
    assertEquals(1, bulkBean.getPropertyValues(bean).length);

    assertEquals(bean.getValue(), bulkBean.getPropertyValues(bean)[0]);

    String newValue = "Hello cglib!";
    bulkBean.setPropertyValues(bean, new Object[] {newValue});
    assertEquals(newValue, bean.getValue());
  }
}
