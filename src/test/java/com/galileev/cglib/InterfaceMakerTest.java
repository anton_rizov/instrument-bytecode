package com.galileev.cglib;

import net.sf.cglib.core.Signature;
import net.sf.cglib.proxy.InterfaceMaker;
import org.junit.Test;
import org.objectweb.asm.Type;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

public class InterfaceMakerTest {
  @Test
  public void test() throws Exception {
    Signature signature = new Signature("foo", Type.DOUBLE_TYPE, new Type[] {Type.INT_TYPE});
    Type[] exceptions = {};
    InterfaceMaker maker = new InterfaceMaker();
    maker.add(signature, exceptions);
    Class iface = maker.create();

    assertEquals(1, iface.getMethods().length);

    Method method = iface.getMethods()[0];
    assertEquals("foo", method.getName());
    assertEquals(double.class, method.getReturnType());
  }
}
