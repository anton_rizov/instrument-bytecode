package com.galileev.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.InvocationHandler;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class InvocationHandlerEnhancerTest {
  private static final String REPLACEMENT = "Hello cglib!";
  private static final String EXCEPTION_MESSAGE = "Do not know what to do.";

  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  private Greeter greeter;

  @Before
  public void initGreeter() {
    greeter = createEnhancedGreeter();
  }

  private Greeter createEnhancedGreeter() {
    Enhancer enhancer = new Enhancer();
    enhancer.setSuperclass(Greeter.class);
    enhancer.setCallback(new InvocationHandler() {
      @Override
      public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getDeclaringClass() != Object.class && method.getReturnType() == String.class) {
          return REPLACEMENT;
        } else {
          throw new RuntimeException(EXCEPTION_MESSAGE);
        }
      }
    });
    return (Greeter) enhancer.create();
  }

  @Test
  public void sayHelloIsEnhanced() {
    assertEquals(REPLACEMENT, greeter.sayHello());
  }

  @Test
  public void toStringIsNotEnhanced() {
    expectedException.expect(RuntimeException.class);
    expectedException.expectMessage(EXCEPTION_MESSAGE);
    //noinspection ResultOfMethodCallIgnored
    greeter.toString();
  }

  @Test
  public void hashCodeIsNotEnhanced() {
    expectedException.expect(RuntimeException.class);
    expectedException.expectMessage(EXCEPTION_MESSAGE);
    //noinspection ResultOfMethodCallIgnored
    greeter.hashCode();
  }

  @Test
  public void notSameClass() {
    assertNotEquals(Greeter.class, greeter.getClass());
  }
}
