package com.galileev.cglib;

import net.sf.cglib.core.CodeGenerationException;
import net.sf.cglib.reflect.MulticastDelegate;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MulticastDelegateTest {
  public interface ValueWriter {
    void setValue(String value);
  }

  public interface ValueReader {
    String getValue();
  }

  public class Box implements ValueReader, ValueWriter {
    private String value;

    @Override
    public String getValue() {
      return value;
    }

    @Override
    public void setValue(String value) {
      this.value = value;
    }
  }

  @Test
  public void test() throws Exception {
    Box first = new Box();
    Box second = new Box();

    ValueWriter box = makeMutiBox(ValueWriter.class, first, second);

    String value = "Hello world!";
    box.setValue(value);
    assertEquals(value, first.getValue());
    assertEquals(value, second.getValue());
  }

  private <T> T makeMutiBox(Class<T> clazz, T first, T second) {
    MulticastDelegate result = MulticastDelegate.create(clazz);
    result = result.add(first);
    result = result.add(second);

    //noinspection unchecked
    return (T) result;
  }

  @Test(expected = IllegalArgumentException.class)
  public void cannotUseClass() {
    MulticastDelegate.create(Box.class);
  }

  @SuppressWarnings("UnusedDeclaration")
  interface TwoMethods {
    void a();
    void b();
  }

  @Test(expected = IllegalArgumentException.class)
  public void interfaceMustHaveOneMethod() {
    MulticastDelegate.create(TwoMethods.class);
  }

  @Test
  public void returnValueFromLastDelegate() {
    Box first = new Box();
    first.setValue("first");

    Box second = new Box();
    second.setValue("second");

    assertNotEquals(first.getValue(), second.getValue());

    ValueReader box = makeMutiBox(ValueReader.class, first, second);
    assertNotEquals(first.getValue(), box.getValue());
    assertEquals(second.getValue(), box.getValue());
  }

  @SuppressWarnings("UnusedDeclaration")
  interface NonPublic {
    void a();
  }

  @Test(expected = CodeGenerationException.class)
  public void interfaceMustBePublic() {
    MulticastDelegate.create(NonPublic.class);
  }
}
