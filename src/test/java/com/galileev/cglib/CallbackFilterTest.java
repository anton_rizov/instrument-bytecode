package com.galileev.cglib;

import net.sf.cglib.proxy.CallbackHelper;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.FixedValue;
import net.sf.cglib.proxy.NoOp;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class CallbackFilterTest {
  public static final String REPLACEMENT = "Hello cglib!";
  
  private Greeter greeter;

  @Before
  public void initGreeter() {
    greeter = createEnhancedGreeter();
  }

  private Greeter createEnhancedGreeter() {
    Enhancer enhancer = new Enhancer();
    enhancer.setSuperclass(Greeter.class);
    CallbackHelper callbackHelper = new CallbackHelper(Greeter.class, new Class[0]) {
      @Override
      protected Object getCallback(Method method) {
        if (!"sayHello".equals(method.getName()))
          return NoOp.INSTANCE;
        return new FixedValue() {
          @Override
          public Object loadObject() throws Exception {
            return REPLACEMENT;
          }
        };
      }
    };
    enhancer.setCallbackFilter(callbackHelper);
    enhancer.setCallbacks(callbackHelper.getCallbacks());
    return (Greeter) enhancer.create();
  }

  @Test
  public void sayHelloIsEnhanced() {
    assertEquals(REPLACEMENT, greeter.sayHello());
  }

  @Test
  public void toStringIsNotEnhanced() {
    assertNotEquals(REPLACEMENT, greeter.toString());
  }

  @Test
  public void hashCodeWorks() {
    //noinspection ResultOfMethodCallIgnored
    greeter.hashCode();
  }

  @Test
  public void fieldsForCallbacksAreDynamicallyGenerated() {
    int inEnhanced = fieldsCount(greeter.getClass());
    int inOriginal = fieldsCount(Greeter.class);
    assertTrue(inEnhanced > inOriginal);
  }

  private int fieldsCount(Class clazz) {
    return clazz.getDeclaredFields().length;
  }
}

