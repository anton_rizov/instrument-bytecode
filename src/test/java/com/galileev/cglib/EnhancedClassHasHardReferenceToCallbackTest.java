package com.galileev.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.FixedValue;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertTrue;

public class EnhancedClassHasHardReferenceToCallbackTest {
  @Test
  public void test() throws Exception {
    Enhancer enhancer = new Enhancer();
    enhancer.setSuperclass(Greeter.class);
    FixedValue callback = new FixedValue() {
      @Override
      public Object loadObject() throws Exception {
        return "dummy";
      }
    };
    enhancer.setCallback(callback);
    Object greeter = enhancer.create();

    assertTrue(hasReference(greeter, callback));
    assertTrue(hasReference(callback, this));
  }

  private boolean hasReference(Object source, Object target) throws Exception {
    Field field = findFieldReferencingClass(source.getClass().getDeclaredFields(), target.getClass());
    field.setAccessible(true);
    return field.get(source) == target;
  }

  private Field findFieldReferencingClass(Field[] fields, Class clazz) {
    for (Field field : fields) {
      if (field.getType().isAssignableFrom(clazz))
        return field;
    }
    return null;
  }
}
