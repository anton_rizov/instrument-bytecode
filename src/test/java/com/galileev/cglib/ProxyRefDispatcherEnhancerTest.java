package com.galileev.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.ProxyRefDispatcher;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProxyRefDispatcherEnhancerTest {
  private Greeter greeter;
  private int loadObjectInvocationCount=0;
  private Object proxy;

  @Before
  public void initGreeter() {
    greeter = createEnhancedGreeter();
  }

  private Greeter createEnhancedGreeter() {
    Enhancer enhancer = new Enhancer();
    enhancer.setSuperclass(Greeter.class);
    enhancer.setCallback(new ProxyRefDispatcher() {
      @Override
      public Object loadObject(final Object proxy) throws Exception {
        ProxyRefDispatcherEnhancerTest.this.proxy = proxy;
        ++loadObjectInvocationCount;
        return new Greeter();
      }
    });
    return (Greeter) enhancer.create();
  }

  @Test
  public void loadObjectIsNotInvokedBeforeFirstMethodInvocation() {
    assertEquals(0, loadObjectInvocationCount);
    greeter.sayHello();
    assertEquals(1, loadObjectInvocationCount);
  }

  @Test
  public void loadObjectIsInvokedForEveryMethod() {
    greeter.sayHello();
    //noinspection ResultOfMethodCallIgnored
    greeter.hashCode();
    assertEquals(2, loadObjectInvocationCount);
  }

  @Test
  public void loadObjectIsInvokedForEveryMethodInvocation() {
    int n = 3;
    for (int i = 0; i < n; i++)
      greeter.sayHello();
    assertEquals(n, loadObjectInvocationCount);
  }

  @Test
  public void sayHelloIsNotEnhanced() {
    assertEquals(new Greeter().sayHello(), greeter.sayHello());
  }

  @Test
  public void toStringIsNotEnhanced() {
    assertNotEquals(new Greeter().toString(), greeter.toString());
  }

  @Test
  public void notSameClass() {
    assertNotEquals(Greeter.class, greeter.getClass());
  }

  @Test
  public void whatIsProxy() {
    greeter.sayHello();
    assertTrue(proxy instanceof Greeter);
    assertSame(proxy, greeter);

    Object oldProxy = proxy;
    greeter.sayHello();
    assertSame(proxy, oldProxy);
    assertSame(proxy, greeter);
  }

  @Test(expected = java.lang.StackOverflowError.class)
  public void endlessRecursionWhenCallingSameMethodOnProxy() {
    final boolean[] hijacked = {false};
    Enhancer enhancer = new Enhancer();
    enhancer.setSuperclass(Greeter.class);
    enhancer.setCallback(new ProxyRefDispatcher() {
      @Override
      public Object loadObject(final Object proxy) throws Exception {
        return new Greeter() {
          @Override
          public String sayHello() {
            if (hijacked[0])
              return "hijacked :)";
            return ((Greeter)proxy).sayHello();
          }
        };
      }
    });
    Greeter greeter = (Greeter) enhancer.create();
    String original = greeter.sayHello();
    hijacked[0] = true;
    assertNotEquals(original, greeter.sayHello());
  }

  @Test
  public void noProblemCallingOtherMethodOnProxy() {
    final boolean[] hijacked = {false};
    Enhancer enhancer = new Enhancer();
    enhancer.setSuperclass(Greeter.class);
    enhancer.setCallback(new ProxyRefDispatcher() {
      @Override
      public Object loadObject(final Object proxy) throws Exception {
        return new Greeter() {
          @Override
          public String sayHello() {
            if (hijacked[0])
              return "hijacked :)";
            return proxy.toString();
          }
        };
      }
    });
    Greeter greeter = (Greeter) enhancer.create();
    String original = greeter.sayHello();
    hijacked[0] = true;
    assertNotEquals(original, greeter.sayHello());
  }
}
