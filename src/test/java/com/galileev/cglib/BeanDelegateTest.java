package com.galileev.cglib;

import net.sf.cglib.reflect.MethodDelegate;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class BeanDelegateTest {
  interface BeanDelegate {
    String getValueFromDelegate();
  }

  @Test
  public void sameValueAsTarget() throws Exception {
    Bean bean = new Bean();
    bean.setValue("Hello cglib!");
    BeanDelegate delegate = makeDelegate(bean);
    assertEquals(bean.getValue(), delegate.getValueFromDelegate());
  }

  private BeanDelegate makeDelegate(Bean bean) {
    return (BeanDelegate) MethodDelegate.create(bean, "getValue", BeanDelegate.class);
  }

  @Test(expected = RuntimeException.class)
  public void callsTargetMethod() {
    Bean bean = new Bean() {
      @Override
      public String getValue() {
        throw new RuntimeException();
      }
    };
    BeanDelegate delegate = makeDelegate(bean);
    delegate.getValueFromDelegate();
  }

  @SuppressWarnings("UnusedDeclaration")
  static class IdentityFunction {
    public Object apply(Object x) {
      return x;
    }
  }

  interface FunctionDelegate {
    Object apply(Object arg);
  }

  @Test
  public void delegateMethodWithArguments() {
    IdentityFunction f = new IdentityFunction();
    FunctionDelegate delegate = (FunctionDelegate) MethodDelegate.create(f, "apply", FunctionDelegate.class);
    Object x = new Object();
    assertSame(x, delegate.apply(x));
  }
}
