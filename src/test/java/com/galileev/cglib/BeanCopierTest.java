package com.galileev.cglib;

import net.sf.cglib.beans.BeanCopier;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class BeanCopierTest {

  private Bean bean;
  private BeanCopy copy;
  private BeanCopier copier;

  public static class BeanCopy {
    private String value;

    public String getValue() {
      return value;
    }

    public void setValue(String value) {
      this.value = value;
    }
  }

  @Before
  public void setUp() {
    bean = new Bean();
    bean.setValue("Hello cglib!");

    copy = new BeanCopy();

    copier = BeanCopier.create(Bean.class, BeanCopy.class, false);
  }

  @Test
  public void valuesDifferBeforeCopy() {
    assertNotEquals(bean.getValue(), copy.getValue());
  }

  @Test
  public void afterCopyTargetHasSameValue() {
    copier.copy(bean, copy, null);

    assertEquals(bean.getValue(), copy.getValue());
  }

  @Test
  public void noInstanceSharing() throws Exception {
    copy.setValue(bean.getValue() + bean.getValue());
    assertNotEquals(bean.getValue(), copy.getValue());

    bean.setValue(copy.getValue() + copy.getValue());
    assertNotEquals(bean.getValue(), copy.getValue());
  }
}
