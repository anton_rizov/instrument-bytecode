package com.galileev.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.FixedValue;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class FinalMethodsAreNotInterceptedTest {
  static class ClassWithFinalMethod {
    final String finalMethod() {
      return "finalMethod";
    }
  }

  @Test
  public void test() throws Exception {
    Enhancer enhancer = new Enhancer();
    enhancer.setSuperclass(ClassWithFinalMethod.class);
    FixedValue callback = new FixedValue() {
      @Override
      public Object loadObject() throws Exception {
        return "dummy";
      }
    };
    enhancer.setCallback(callback);
    ClassWithFinalMethod x = (ClassWithFinalMethod) enhancer.create();

    assertEquals(new ClassWithFinalMethod().finalMethod(), x.finalMethod());
    assertNotEquals(new ClassWithFinalMethod().finalMethod(), callback.loadObject());
  }
}
