package com.galileev.cglib;

import net.sf.cglib.beans.ImmutableBean;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ImmutableBeanTest {
  private Bean bean;
  private Bean immutable;

  static class Bean {
    private int property;

    public int getProperty() {
      return property;
    }

    public void setProperty(int property) {
      this.property = property;
    }
  }

  @Before
  public void setUp() {
    bean = new Bean();
    bean.setProperty(1);

    immutable = (Bean) ImmutableBean.create(bean);
  }

  @Test
  public void changesAreVisible() {
    assertEquals(bean.getProperty(), immutable.getProperty());
    bean.setProperty(bean.getProperty() + 1);
    assertEquals(bean.getProperty(), immutable.getProperty());
  }

  @Test
  public void directAccessIsAllowed() {
    immutable.property++;
  }

  @Test(expected = IllegalStateException.class)
  public void cannotModifyImmutableBean() {
    immutable.setProperty(3);
  }
}
