package com.galileev.cglib;

import net.sf.cglib.proxy.Mixin;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MixinTest {
  public interface Interface1 {
    String first();
  }

  public interface Interface2 {
    String second();
  }

  public class Class1 implements Interface1 {
    @Override
    public String first() {
      return "first";
    }
  }

  public class Class2 implements Interface2 {
    @Override
    public String second() {
      return "second";
    }
  }

  public interface MixinInterface extends Interface1, Interface2 {
  }

  @Test
  public void testMixin() throws Exception {
    Mixin mixin = Mixin.create(new Class[]{Interface1.class, Interface2.class, MixinInterface.class},
                               new Object[]{new Class1(), new Class2()});
    MixinInterface mixinDelegate = (MixinInterface) mixin;
    assertEquals("first", mixinDelegate.first());
    assertEquals("second", mixinDelegate.second());
  }

  interface Interface3 {
    String first();
  }

  static class Class3 implements Interface3 {
    @Override
    public String first() {
      return "first3";
    }
  }

  interface MixinInterface13 extends Interface1, Interface3 {
  }

  @Test
  public void sameMethodFromTwoInterfaces() throws Exception {
    Mixin mixin = Mixin.create(new Class[]{MixinInterface13.class},
                               new Object[]{new Class1(), new Class3()});
    MixinInterface13 mixinDelegate = (MixinInterface13) mixin;
    assertEquals("first", mixinDelegate.first());
  }

  @Test
  public void triesToCastInOrder() throws Exception {
    Mixin mixin = Mixin.create(new Class[]{Interface3.class, Interface1.class, MixinInterface13.class},
                               new Object[]{new Class3(), new Class1()});
    MixinInterface13 mixinDelegate = (MixinInterface13) mixin;
    assertEquals("first3", mixinDelegate.first());
  }

  @Test(expected = java.lang.ClassCastException.class)
  public void instancesShouldMatchInterfaces() throws Exception {
    Mixin mixin = Mixin.create(new Class[]{Interface1.class, Interface3.class, MixinInterface13.class},
                               new Object[]{new Class3(), new Class1()});
    MixinInterface13 mixinDelegate = (MixinInterface13) mixin;
    assertEquals("first", mixinDelegate.first());
  }
}
