package com.galileev.cglib;

import net.sf.cglib.proxy.Dispatcher;
import net.sf.cglib.proxy.Enhancer;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DispatcherEnhancerTest {
  private Greeter greeter;
  private int loadObjectInvocationCount=0;

  @Before
  public void initGreeter() {
    greeter = createEnhancedGreeter();
  }

  private Greeter createEnhancedGreeter() {
    Enhancer enhancer = new Enhancer();
    enhancer.setSuperclass(Greeter.class);
    enhancer.setCallback(new Dispatcher() {
      @Override
      public Object loadObject() throws Exception {
        ++loadObjectInvocationCount;
        return new Greeter();
      }
    });
    return (Greeter) enhancer.create();
  }
  @Test
  public void loadObjectIsNotInvokedBeforeFirstMethodInvocation() {
    assertEquals(0, loadObjectInvocationCount);
    greeter.sayHello();
    assertEquals(1, loadObjectInvocationCount);
  }


  @Test
  public void loadObjectIsInvokedForEveryMethodInvocation() {
    int n = 3;
    for (int i = 0; i < n; i++)
      greeter.sayHello();
    assertEquals(n, loadObjectInvocationCount);
  }

  @Test
  public void sayHelloIsNotEnhanced() {
    assertEquals(new Greeter().sayHello(), greeter.sayHello());
  }

  @Test
  public void toStringIsNotEnhanced() {
    assertNotEquals(new Greeter().toString(), greeter.toString());
  }

  @Test
  public void notSameClass() {
    assertNotEquals(Greeter.class, greeter.getClass());
  }
}
