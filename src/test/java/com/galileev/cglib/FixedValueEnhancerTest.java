package com.galileev.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.FixedValue;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class FixedValueEnhancerTest {
  private static final String REPLACEMENT = "Hello cglib!";

  private Greeter greeter;

  @Before
  public void initGreeter() {
    greeter = createEnhancedGreeter();
  }

  private Greeter createEnhancedGreeter() {
    Enhancer enhancer = new Enhancer();
    enhancer.setSuperclass(Greeter.class);
    enhancer.setCallback(new FixedValue() {
      @Override
      public Object loadObject() throws Exception {
        return REPLACEMENT;
      }
    });
    return (Greeter) enhancer.create();
  }

  @Test
  public void sayHelloIsEnhanced() {
    assertEquals(REPLACEMENT, greeter.sayHello());
  }

  @Test
  public void toStringIsEnhanced() {
    assertEquals(REPLACEMENT, greeter.toString());
  }

  @Test(expected = ClassCastException.class)
  public void hashCodeIsEnhanced() {
    //noinspection ResultOfMethodCallIgnored
    greeter.hashCode();
  }

  @Test
  public void notSameClass() {
    assertNotEquals(Greeter.class, greeter.getClass());
  }
}
